using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;

namespace FTPClient
{
    class client
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" [Waiting...]");
            TcpClient client = new TcpClient("192.168.43.134", 5656);
            StreamWriter writer = new StreamWriter(client.GetStream());
            Console.WriteLine(" [Connected to Server]");

            try
            {
                Console.Write("\n [Please enter a full file path] ");
                string directoryFile = Console.ReadLine();

                Console.WriteLine(" [Waiting. Sending file.]");

                byte[] bytes = File.ReadAllBytes(directoryFile);

                writer.WriteLine(bytes.Length.ToString());
                writer.Flush();

                writer.WriteLine(directoryFile);
                writer.Flush();

                client.Client.SendFile(directoryFile);
                Console.WriteLine(" [File Sent]");
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            Console.Read();
        }
    }
}