import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.Buffer;

public class Server extends Thread {
	
	private ServerSocket ss;
	
	public Server(int port) {
		try {
			ss = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void start() {
		while (true) {
			try {
				Socket ServerSock = ss.accept();
				saveFile(ServerSock);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void saveFile(Socket ServerSock) throws IOException {   
        DataInputStream dis = new DataInputStream(ServerSock.getInputStream());
        BufferedReader fromServer = new BufferedReader(new InputStreamReader(ServerSock.getInputStream())); 
        
		FileOutputStream fos = new FileOutputStream("a.zip");
		byte[] buffer = new byte[4096];
		
		int filesize = 1024 * 1000000000; // Send file size in separate msg
		int read = 0;
		int totalRead = 0;
		int remaining = filesize;
		while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			totalRead += read;
			remaining -= read;
			System.out.println(totalRead + " bytes.");
			fos.write(buffer, 0, read);
		}
		
		fos.close();
		dis.close();
	}
	
	public static void main(String[] args) {
		Server server = new Server(5656);
		server.start();
	}
}